// simply supported beam by Beam2D element
/*      y
* z ^  /         ^ force or moment                 ^ z
*   | /  e1      |                 2.0  ____       |
*   |------------- --> x         width |____|      |----> y
*   p1           p2                    height, 4.0
*/

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#define private public
#define protected public
#include <element.h>
#undef private
#undef protected

using namespace std;

inline void printStdArray6d(const StdArray6d &arr)
{
    for (int i = 0; i < 6; i++)
    {
        cout << arr[i] << ", ";
    }
    cout << endl;
}

inline void printStdArray6d(const StdArray6d* arr)
{
    for (int i = 0; i < 6; i++)
    {
        cout << (*arr)[i] << ", ";
    }
    cout << endl;
}


int main()
{
    // create particles
    Particle* p1 = new Particle(1, 0, 0);
    Particle* p2 = new Particle(2, 10, 0);

    // create section
    Rectangle* sect = new Rectangle(1, 2, 4);

    // create material
    LinearElastic* mat = new LinearElastic(1);
    mat->setDensity(10);
    mat->setE(1000);
    mat->setNu(0.31);

    // create elements
    Beam3D* e1 = new Beam3D(1, p1, p2, mat, sect);

    // create DOF
    DOF d1 = {.key={true, true, true, true, true, true},
              .val={0, 0, 0, 0, 0, 0}};

    p1->constraintDof(d1);

    // force
    StdArray6d force = {0, 0, 0, 0, 0, 0};
    force[0] = 10;
    // force = {10, 10, 10, 10, 10, 10};

    // solving parameters
    double h = 0.001;
    double zeta = 0.5;
    int nStep = ceil(50 / h);

    for (int i = 0; i < nStep; i++)
    {
        if (i == 0)
        {
            p2->setForce(force);
            p1->solveFirstStep(h, zeta);
            p2->solveFirstStep(h, zeta);
            e1->setParticleForce();
        }
        else
        {
            p1->solve(h, zeta);
            p2->solve(h, zeta);
            p1->clearForce();
            p2->clearForce();
            p2->setForce(force);
            e1->setParticleForce();
        }
        p1->constraintDof(d1);
    }
    // results
    cout << "------- particle force ------" << endl;
    cout << "p1: ";
    printStdArray6d(p1->force());
    cout << "p2: ";
    printStdArray6d(p2->force());

    cout << "------- particle displace ------" << endl;
    cout << "p1: ";
    printStdArray6d(p1->displace());
    cout << "p2: ";
    printStdArray6d(p2->displace());


    cout << "------- element force ------" << endl;
    cout << "e1, fx, sfy, sfz, mx, my, mz: " << endl;
    cout << "emfi: " << e1->emfi_.fx << ", " << e1->emfi_.sfy << ", ";
    cout << e1->emfi_.sfz << ", " << e1->emfi_.mx << ", " << e1->emfi_.my;
    cout << ", " << e1->emfi_.mz << endl;
    cout << "emfj: " << e1->emfj_.fx << ", " << e1->emfj_.sfy << ", ";
    cout << e1->emfj_.sfz << ", " << e1->emfj_.mx << ", " << e1->emfj_.my;
    cout << ", " << e1->emfj_.mz << endl;

    // cout << "e1: " << e1->force_.transpose() << endl;

    delete p1, p2;
    delete mat, sect;
    delete e1;
}

/* result summery
* force = {10, 0, 0, 0, 0, 0};//small defromation, sect(2, 4)
* e1: emfi = {-9.99996, 0, 0, 0, 0, 0},
*     emfj = { 9.99996, 0, 0, 0, 0, 0}
*
* force = {10, 0, 0, 0, 0, 0};//small defromation, sect(20, 40)
* e1: emfi = {-9.99996, 0, 0, 0, 0, 0},
*     emfj = {9.99996, 0, 0, 0, 0, 0}
*
* force = {0, 10, 0, 0, 0, 0}; //small defromation, sect(2, 4)
* e1: emfi = {-0.312318, -9.99509, 0, 0, 0, -99.9548},
*     emfj = {0.312318, 9.99509, 0, 0, 0, -5.70216e-06}
*
* force = {0, 10, 0, 0, 0, 0}; //small defromation, sect(20, 40)
* e1: emfi = {-3.12812e-05, -9.99998, 0, 0, 0, -99.9997},
*     emfj = {3.12812e-05, 9.99998, 0, 0, 0, -9.75185e-05}
×
* force = {0, 0, 10, 0, 0, 0}; //large defromation, sect(2, 4)
* e1: emfi = {-1.08695, 0, -8.71781, 0, 87.1286, 0},
*     emfj = {1.08695, 0, 8.71781, 0, 0.0612968, 0}
*
* force = {0, 0, 10, 0, 0, 0}; //small defromation, sect(20, 40)
* e1: emfi = {-0.000124999, 0, -9.99999, 0, 99.9998, 0},
*     emfj = {0.000124999, 0, 9.99999, 0, 0.000180793, 0}
*
* force = {0, 0, 0, 10, 0, 0}; // small deformation, sect(2, 4)
* e1: emfi = {0, 0, 0, -10, 0, 0},
*     emfj = {0, 0, 0, 10, 0, 0}
*
* force = {0, 0, 0, 0, 10, 0}; //large defromation, sect(2, 4)
* e1: emfi = {-4.29908e-05, 0, -0.187565, 0, -8.11499, 0},
*     emfj = { 4.29908e-05, 0, 0.187565, 0, 9.99064, 0}
*
* force = {0, 0, 0, 0, 10, 0}; // small deformation, sect(20, 40)
* e1: emfi = {7.13024e-20, 0, -5.42381e-06, 0, -9.99994, 0},
*     emfj = {-7.13024e-20, 0, 5.42381e-06, 0, 9.99999, 0}
*
* force = {0, 0, 0, 0, 0, 10}; small defromation, sect(2, 4)
* e1: emfi = { -5.81272e-10, 4.12803e-06, 0, 0, 0, -9.99996},
*     emfj = {5.81272e-10, -4.12803e-06, 0, 0, 0, 10}
*
* force = {0, 0, 0, 0, 0, 10}; // small deformation, sect(20, 40)
* e1: emfi = {4.05594e-18, 7.82526e-07, 0, 0, 0, -9.99996},
*     emfj = {-4.05594e-18, -7.82526e-07, 0, 0, 0, 9.99997}
*
* force = {10, 10, 10, 10, 10, 10}; // large deformation, sect(2, 4)
* e1: emfi = {-12.1034, -7.83379, -0.119789, -14.2213, -9.42306, -84.9067},
*     emfj = {12.1034, 7.83379, 0.119789, 14.2213, 10.6227, 6.45153}
*
* force = {10, 10, 10, 10, 10, 10}; // small deformation, sect(20, 40)
* e1: emfi = {-10.0132, -10.0007, -4.59407, -5.07725, 17.9664, -109.999},
*     emfj = {10.0132, 10.0007, 4.59407, 5.07725, 27.9749, 9.99134}
*/