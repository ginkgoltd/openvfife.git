#include <fstream>
#include <material.h>
using namespace std;


int main()
{
    double m = 0;
    cout << "Please enter hardening parameter m: " << endl;
    cin >> m;

    double E = 2.06E11;
    double Et = E / 100;
    double yield_stress = 235E6;
    UniBilinear mat = UniBilinear(1, E, 2.06E9, yield_stress, m);
    mat.setNu(0.31);
    mat.setDensity(7850);
    mat.info();

    double ds = 0.00001;
    double stress = 0, strain = 0, alpha = 0, k = 0, eps = 0;

    // alpha = yield_stress / (E * Et / (E - Et));
    // k = yield_stress;
    fstream fout;
    fout.open("unibilinear" + to_string(m) + ".csv", ios::out | ios::trunc);

    for (int i = 0; i < 1000; i++)
    {
        double E = mat.Eq(ds, &stress, &strain, &eps, &alpha, &k);
        fout << strain << ", " << stress << endl;
    }

    ds = -ds;
    for (int i = 0; i < 3000; i++)
    {
        double E = mat.Eq(ds, &stress, &strain, &eps, &alpha, &k);
        fout << strain << ", " << stress << endl;
    }

    ds = -ds;
    for (int i = 0; i < 2000; i++)
    {
        double E = mat.Eq(ds, &stress, &strain, &eps, &alpha, &k);
        fout << strain << ", " << stress << endl;
    }

    for (int i = 0; i < 2000; i++)
    {
        if (i % 2 == 0)
        {
            ds = -ds;
        }
        double E = mat.Eq(ds, &stress, &strain, &eps, &alpha, &k);
        fout << strain << ", " << stress << endl;
    }
    fout.close();

    return 0;
}
