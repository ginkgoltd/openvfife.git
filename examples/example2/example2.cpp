#include "../../headers/structsystem.h"

using namespace std;


int main()
{
    // create system
    StructSystem system = StructSystem(1);
    system.setJobname("example2");

    // setting damping coefficient
    double zeta = 0.5;
    // cout << "Please enter a damping coefficient(>=0): " << endl;
    // cin >> zeta;

    // setting time step
    double h = 0.0;
    cout << "Please enter a time step(>=0): " << endl;
    cin >> h;

    // solve parameters
    double endTime = 50.0;
    // double h = 1.0e-3;
    double p = 100;

    // create particles
    Particle * p1 = new Particle(1, 0, 0);
    Particle * p2 = new Particle(2, 10, 0);
    system.addParticle(p1);
    system.addParticle(p2);

    // create material
    LinearElastic mat = LinearElastic(1);
    mat.setE(1.0E6);
    mat.setDensity(10);

    // create section
    CustomSectionParas paras = {.A=1, .Sy=0, .Sz=0, .SHy=0, .SHz=0,
                                .CGy=0, .CGz=0.0, .Iyy=10, .Izz=10, .Iyz=10};
    CustomSection sect = CustomSection(1, paras);

    // create elements
    Link2D* e = new Link2D(1, p1, p2, &mat, &sect);
    system.addElement(e);

    // constraints
    DOF c1 = {.key = {true, true, true, true, true, true},
              .val = {0, 0, 0, 0, 0, 0}};
    DOF c2 = {.key = {false, true, true, true, true, true},
              .val = {0, 0, 0, 0, 0, 0}};
    system.addConstraint(1, c1);
    system.addConstraint(2, c2);

    // save model
    string path = system.workdir() + "/" + system.jobname() + "/model";
    system.saveModel(path);
    system.info();      // print structsystem information

    StdArray6d f1 = {100, 0, 0, 0, 0, 0};
    int nStep = ceil(endTime/h);
    cout << nStep << endl;
    int interval = ceil(0.1/h);
    for (int i = 0; i <= nStep; i++)
    {
        if (i == 0)
        {
            system.addExternalForce(2, f1);            // add external force
            system.solve(h, zeta, true);
            system.setInternalForce();
        }
        else
        {
            system.solve(h, zeta);
            system.clearParticleForce();
            system.setExternalForce(2, f1);
            system.setInternalForce();
        }

        // save results
        if (i % interval == 0)
        {
            string path = system.workdir() + "/" + system.jobname() + "/" +
                          to_string(i*h);
            system.saveParticleResult(path);
            system.saveElementResult(path);
            system.saveSupportReact(path);
        }
    }

    system.releaseContainers();
    return 0;
}